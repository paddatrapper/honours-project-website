---
title: Lodestar
date: 2019-10-01T12:20:33+02:00
---
# Overview

Lodestar is an ontology-based text annotation platform. It provides a reusable
digital textbook platform that will annotate uploaded textbooks. These
annotations consist of definitions and related concepts extracted from the
ontology. They are presented to the user in parallel with the textbook.

## Operation

{{% figure src="static/pipeline.svg" alt="Pipeline" caption="Textbook processing pipeline" width="70%" %}}

Textbooks can be uploaded as single HTML files or as a PDF document. The Source
Parser then converts these into a standard HTML format that the annotation
system uses to detect terms found in the ontology. Once these are annotated, the
completed HTML document is passed to the renderer where users can view it.

An example Lodestar site can be viewed at https://lodestar.paddatrapper.com. The
demo is reset every 2 hours and does not persist changes. Log in using the
username and password "admin". This site is not guaranteed to be available after
31 December 2019. The code is available on
[GitLab](https://gitlab.com/paddatrapper/lodestar).

{{% figure src="static/screenshot.png" alt="Screenshot" caption="Lodestar screenshot" width="70%" %}}

# Final Project Paper

The final paper can be viewed online [here](static/paper.pdf). This paper gives
a technical overview of Lodestar and how it performs text annotation using
ontologies. It also compares the accuracy and recall for several versions of
the text annotation matching system.

# Other Documents

The [literature review](static/lit-review.pdf), [proposal](/proposal.pdf) and
[poster](/images/poster.svg) are also available online.

This project was completed in conjuction with
[Automated Question Generation]({{% relref "questions/index.md" %}}).
